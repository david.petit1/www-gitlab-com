## Basic setup

1. [ ] Change this issue title to include your name: `Trainee Quality Maintainer: [Your Name]`.
1. [ ] Choose which Quality project(s) you would like to become a maintainer:
   - [ ] [`GitLab (/qa)`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/qa)
   - [ ] [`GitLab-QA`](https://gitlab.com/gitlab-org/gitlab-qa)
   - [ ] [`GitLab Triage`](https://gitlab.com/gitlab-org/gitlab-triage/)
   - [ ] [`Triage Ops`](https://gitlab.com/gitlab-org/quality/triage-ops/)
   - [ ] [`GitLab Subscriptions App (/qa)`](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/qa/)
1. [ ] The requirements for each project is different, please make sure to read the
[maintainer section in the Quality handbook](https://about.gitlab.com/handbook/engineering/quality/project-management/#reviewers-and-maintainers).
1. [ ] Add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer)
on the [team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml).
1. [ ] Mention your manager in this issue for awareness.

## Working towards becoming a maintainer

This is not a checklist, but guidelines that will help you become a maintainer.
Remember that there is no specific timeline on this, and that you should work
together with your manager and current maintainers.

In general authoring and reviewing 3 - 10 MRs that demonstrate good overall understanding
of existing codebase and framework is required. You could seek out more opportunities to work on
framework improvements by asking on the `#quality` Slack channel.

Your reviews should aim to cover maintainer responsibilities as well as reviewer
responsibilities. Your approval means you think it is ready to merge.

### Merge Request Authored

<!--
  Please list all MRs you have authored during or before your time as a Trainee Maintainer,
  for example: 
  1. Framework Improvements: <link_to_mr>
  2. Quality e2e Tests: <link_to_mr>
-->

### Merge Request Reviewed

After each MR that you reviewed is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:

- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:

- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(Maintainer who reviewed this merge request) Please add feedback.
```

**Note:** Do not include reviews of security MRs because review feedback might
reveal security issue details.

**Tip:** There are [tools](https://about.gitlab.com/handbook/source/handbook/tools-and-tips/#trainee-maintainer-issue-upkeep) available to assist with this task.

It is your responsibility to set up any necessary meetings to discuss your
progress with current maintainers, as well as your manager. These can be at any
increment that is right for you.

## When you're ready to make it official

The trainee should also feel free to discuss their progress with their manager
or any maintainer at any time.

1. [ ] Create a merge request for [team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml)
proposing yourself as a quality maintainer for the chosen project.
1. [ ] Keep reviewing, start merging 😃

/label ~"trainee maintainer" ~"Quality Department"
