---
layout: handbook-page-toc
title: "Startups Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab for Startups Program Overview

The GitLab for Startups program is meant to help qualifying startups access to our top tiers and 50,000 CI minutes for free for 12 months. 

This program is currently only available to recent Y Combinator startups. 

### Benefits
 * Free Ultimate or Gold license for a year
 * Optional paid support at a discount (95% off, $4.95 per user per month)

### Eligibility
The program is open to startups which meet all eligibility requirements:

 * Members of the current or two most recent YCombinator batches (currently W2020, S2019, and W2019)
 * Less than $3M in funding
 * Usage ping or any other product analytics service must be enabled in GitLab

### Additional information
 * This offer is limited to 12 months
 * The number of seats is the number of different users that will use this license during the next year.
 * If you have any additional questions regarding this program, feel free to reach us at startups@gitlab.com.

Program application page: https://about.gitlab.com/solutions/startups/
